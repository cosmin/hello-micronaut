# Hello Micronaut

An example project to explore [Micronaut](https://micronaut.io/)
as an alternative to [Spring](https://spring.io/) for building microservices.

This is a small Java service that exposes a RESTful API
over CRUD operations on an employee model.

## The model

* Employee
  * name
  * position
  * superior (link to another employee with a management position)
  * start date
  * end date

## How to run

1. Run a Docker container with PostgreSQL
    ```shell
    docker run -it --rm \
        -p 5432:5432 \
        -e POSTGRES_USER=dbuser \
        -e POSTGRES_PASSWORD=theSecretPassword \
        -e POSTGRES_DB=micronaut \
        postgres:13.2-alpine
    ```
1. Run the project
    ```shell
    ./gradlew run
    ```

The tests run on [H2](https://h2database.com/).

## How to use

An example scenario would be:
1. Add two positions, "worker" and "manager"
    ```shell
    curl --location --request PUT 'http://localhost:8080/position' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "name": "worker"
    }'
    ```
    ```shell
    curl --location --request PUT 'http://localhost:8080/position' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "name": "manager"
    }'
    ```
1. Add a manager
    ```shell
    curl --location --request PUT 'http://localhost:8080/employee' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "name": "Robinson",
        "position": 2,
        "startDate": "2021-04-10",
        "endDate": "2021-12-31"
    }'
    ```
1. Add a worker
    ```shell
    curl --location --request PUT 'http://localhost:8080/employee' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "name": "Friday",
        "position": 1,
        "superior": 3,
        "startDate": "2021-04-10",
        "endDate": "2021-12-31"
    }'
    ```
1. List employees
    ```shell
    curl --location --request GET 'http://localhost:8080/employee?sort=name&order=desc&offset=0&max=2'
    ```
    * All query parameters are optional.
1. List only employees with a specific position
    ```shell
    curl --location --request GET 'http://localhost:8080/employee?position=1&sort=name&order=desc&offset=0&max=2'
    ```

## API

* [YAML](http://localhost:8080/swagger/hello-micronaut-v0.1.yml)
* [Swagger-ui](http://localhost:8080/swagger-ui)
* [Redoc](http://localhost:8080/redoc)
* [RapiDoc](http://localhost:8080/rapidoc)
