package ro.hume.cosmin.hello.micronaut.api;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import lombok.extern.slf4j.Slf4j;
import ro.hume.cosmin.hello.micronaut.command.EmployeeSaveCommand;
import ro.hume.cosmin.hello.micronaut.dao.FilterAndSortArguments;
import ro.hume.cosmin.hello.micronaut.domain.Employee;
import ro.hume.cosmin.hello.micronaut.service.EmployeeService;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;

import static io.micronaut.http.HttpStatus.CREATED;

@Controller("/employee")
@Slf4j
public class EmployeeController {

    @Inject
    private EmployeeService employeeService;

    @Put
    @Status(CREATED)
    public Employee createEmployee(EmployeeSaveCommand cmd) {
        var employee = employeeService.create(cmd);
        log.info("Created employee {}", employee.getId());
        return employee;
    }

    @Get("/{id}")
    public Employee getEmployee(Long id) {
        return employeeService.get(id)
                .orElse(null);
    }

    @Get(value = "{?args*}")
    public List<Employee> list(@Valid FilterAndSortArguments args) {
        return employeeService.list(args);
    }

    @Delete("/{id}")
    public HttpResponse<Employee> delete(Long id) {
        log.info("Deleting employee {}", id);
        employeeService.delete(id);
        return HttpResponse.noContent();
    }
}
