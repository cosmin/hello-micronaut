package ro.hume.cosmin.hello.micronaut.dao;

import io.micronaut.transaction.annotation.ReadOnly;
import ro.hume.cosmin.hello.micronaut.ApplicationConfiguration;
import ro.hume.cosmin.hello.micronaut.domain.Position;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Singleton
public class DefaultPositionRepository implements PositionRepository {

    @Inject
    private EntityManager entityManager;

    @Inject
    private ApplicationConfiguration applicationConfiguration;

    @Override
    @ReadOnly
    public Optional<Position> findById(@NotNull Long id) {
        return Optional.ofNullable(entityManager.find(Position.class, id));
    }

    @Override
    @Transactional
    public Position save(@NotBlank String name) {
        Position position = Position.builder().name(name).build();
        entityManager.persist(position);
        return position;
    }

    @Override
    @Transactional
    public void deleteById(@NotNull Long id) {
        findById(id).ifPresent(entityManager::remove);
    }
}
