package ro.hume.cosmin.hello.micronaut.dao;

import ro.hume.cosmin.hello.micronaut.domain.Employee;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface EmployeeRepository {

    Optional<Employee> findById(@NotNull Long id);

    Employee save(@NotNull Employee employee);

    List<Employee> findAll(@NotNull FilterAndSortArguments args);

    void deleteById(@NotNull Long id);
}
