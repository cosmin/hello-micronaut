package ro.hume.cosmin.hello.micronaut.command;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDate;

import static lombok.AccessLevel.PRIVATE;

@Data
@Jacksonized
@Builder
@FieldDefaults(level = PRIVATE)
public class EmployeeSaveCommand {

    String name;
    Long position;
    Long superior;
    LocalDate startDate;
    LocalDate endDate;
}
