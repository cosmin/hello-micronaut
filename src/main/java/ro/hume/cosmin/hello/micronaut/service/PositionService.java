package ro.hume.cosmin.hello.micronaut.service;

import ro.hume.cosmin.hello.micronaut.domain.Position;

import java.util.Optional;

public interface PositionService {

    Position create(String name);

    Optional<Position> get(Long id);

    void delete(Long id);
}
