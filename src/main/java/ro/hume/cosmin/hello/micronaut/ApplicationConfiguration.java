package ro.hume.cosmin.hello.micronaut;

import javax.validation.constraints.NotNull;

public interface ApplicationConfiguration {

    @NotNull Integer getPageSize();
}
