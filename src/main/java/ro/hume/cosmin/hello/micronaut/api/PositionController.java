package ro.hume.cosmin.hello.micronaut.api;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import ro.hume.cosmin.hello.micronaut.command.PositionSaveCommand;
import ro.hume.cosmin.hello.micronaut.domain.Position;
import ro.hume.cosmin.hello.micronaut.service.PositionService;

import javax.inject.Inject;
import javax.validation.Valid;

import static io.micronaut.http.HttpStatus.CREATED;

@Controller("/position")
public class PositionController {

    @Inject
    private PositionService positionService;

    @Put
    @Status(CREATED)
    public Position createPosition(@Body @Valid PositionSaveCommand cmd) {
        return positionService.create(cmd.getName());
    }

    @Get("/{id}")
    public Position getPosition(Long id) {
        return positionService.get(id)
                .orElse(null);
    }

    @Delete("/{id}")
    public HttpResponse<Position> delete(Long id) {
        positionService.delete(id);
        return HttpResponse.noContent();
    }
}
