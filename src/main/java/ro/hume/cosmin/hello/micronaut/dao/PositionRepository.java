package ro.hume.cosmin.hello.micronaut.dao;

import ro.hume.cosmin.hello.micronaut.domain.Position;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;

public interface PositionRepository {

    Optional<Position> findById(@NotNull Long id);

    Position save(@NotBlank String name);

    void deleteById(@NotNull Long id);
}
