package ro.hume.cosmin.hello.micronaut.command;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@Introspected
public class PositionSaveCommand {

    @NotBlank
    private String name;
}
