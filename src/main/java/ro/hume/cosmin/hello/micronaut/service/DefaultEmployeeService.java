package ro.hume.cosmin.hello.micronaut.service;

import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import ro.hume.cosmin.hello.micronaut.dao.EmployeeRepository;
import ro.hume.cosmin.hello.micronaut.command.EmployeeSaveCommand;
import ro.hume.cosmin.hello.micronaut.dao.FilterAndSortArguments;
import ro.hume.cosmin.hello.micronaut.domain.Employee;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;

@ExecuteOn(TaskExecutors.IO)
@Singleton
@Slf4j
public class DefaultEmployeeService implements EmployeeService {

    @Inject
    private EmployeeRepository employeeRepository;

    @Inject
    private PositionService positionService;

    @Inject
    private ModelMapper modelMapper;

    @Override
    public Employee create(EmployeeSaveCommand cmd) {
        var employee = employeeFrom(cmd);
        log.info("Creating employee {}", employee);
        return employeeRepository.save(employee);
    }

    private Employee employeeFrom(EmployeeSaveCommand cmd) {
        var employee = modelMapper.map(cmd, Employee.class);

        var position = positionService.get(cmd.getPosition()).orElseThrow();
        employee.setPosition(position);

        if (cmd.getSuperior() != null) {
            var superior = employeeRepository.findById(cmd.getSuperior()).orElseThrow();
            employee.setSuperior(superior);
        }

        return employee;
    }

    @Override
    public Optional<Employee> get(Long id) {
        return employeeRepository.findById(id);
    }

    @Override
    public List<Employee> list(FilterAndSortArguments args) {
        return employeeRepository.findAll(args);
    }

    @Override
    public void delete(Long id) {
        employeeRepository.deleteById(id);
    }
}
