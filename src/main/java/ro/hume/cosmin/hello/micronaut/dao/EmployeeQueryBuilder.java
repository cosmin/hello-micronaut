package ro.hume.cosmin.hello.micronaut.dao;

import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;

@Singleton
public class EmployeeQueryBuilder implements QueryBuilder {

    private static final List<String> VALID_PROPERTY_NAMES = List.of("id", "name");

    public String findAllQuery(FilterAndSortArguments args) {
        String qlString = "SELECT e FROM Employee AS e";
        Optional<Long> positionFilter = args.getPosition();
        Optional<String> sort = args.getSort();
        Optional<String> order = args.getOrder();
        if (positionFilter.isPresent()) {
            qlString += " WHERE e.position = " + positionFilter.get();
        }
        if (sort.isPresent() && VALID_PROPERTY_NAMES.contains(sort.get())) {
            qlString += " ORDER BY e." + sort.get();
            if (order.isPresent()) {
                qlString += " " + order.get().toUpperCase();
            }
        }
        return qlString;
    }
}
