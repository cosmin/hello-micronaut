package ro.hume.cosmin.hello.micronaut.service;

import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import ro.hume.cosmin.hello.micronaut.dao.PositionRepository;
import ro.hume.cosmin.hello.micronaut.domain.Position;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;

@ExecuteOn(TaskExecutors.IO)
@Singleton
public class DefaultPositionService implements PositionService {

    @Inject
    private PositionRepository positionRepository;

    @Override
    public Position create(String name) {
        return positionRepository.save(name);
    }

    @Override
    public Optional<Position> get(Long id) {
        return positionRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        positionRepository.deleteById(id);
    }
}
