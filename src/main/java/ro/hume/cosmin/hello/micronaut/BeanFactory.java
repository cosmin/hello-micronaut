package ro.hume.cosmin.hello.micronaut;

import io.micronaut.context.annotation.Factory;
import org.modelmapper.ModelMapper;

import javax.inject.Singleton;

@Factory
public class BeanFactory {

    @Singleton
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
