package ro.hume.cosmin.hello.micronaut;

import io.micronaut.context.annotation.ConfigurationProperties;

import javax.validation.constraints.NotNull;

@ConfigurationProperties("application")
public class ApplicationConfigurationProperties implements ApplicationConfiguration {

    private static final Integer DEFAULT_PAGE_SIZE = 10;

    @NotNull
    private Integer pageSize = DEFAULT_PAGE_SIZE;

    @Override
    @NotNull
    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(@NotNull Integer pageSize) {
        this.pageSize = pageSize;
    }
}
