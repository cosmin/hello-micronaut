package ro.hume.cosmin.hello.micronaut.dao;

import io.micronaut.transaction.annotation.ReadOnly;
import ro.hume.cosmin.hello.micronaut.ApplicationConfiguration;
import ro.hume.cosmin.hello.micronaut.domain.Employee;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Singleton
public class DefaultEmployeeRepository implements EmployeeRepository {

    @Inject
    private EntityManager entityManager;

    @Inject
    private QueryBuilder employeeQueryBuilder;

    @Inject
    private ApplicationConfiguration applicationConfiguration;

    @Override
    @ReadOnly
    public Optional<Employee> findById(@NotNull Long id) {
        return Optional.ofNullable(entityManager.find(Employee.class, id));
    }

    @ReadOnly
    public List<Employee> findAll(@NotNull FilterAndSortArguments args) {
        String qlString = employeeQueryBuilder.findAllQuery(args);
        TypedQuery<Employee> query = entityManager.createQuery(qlString, Employee.class);
        query.setMaxResults(args.getMax().orElseGet(applicationConfiguration::getPageSize));
        args.getOffset().ifPresent(query::setFirstResult);

        return query.getResultList();
    }

    @Override
    @Transactional
    public Employee save(@NotNull Employee employee) {
        entityManager.persist(employee);
        return employee;
    }

    @Override
    @Transactional
    public void deleteById(@NotNull Long id) {
        findById(id).ifPresent(entityManager::remove);
    }
}
