package ro.hume.cosmin.hello.micronaut.dao;

public interface QueryBuilder {

    String findAllQuery(FilterAndSortArguments args);
}
