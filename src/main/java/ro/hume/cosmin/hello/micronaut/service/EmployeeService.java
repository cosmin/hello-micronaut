package ro.hume.cosmin.hello.micronaut.service;

import ro.hume.cosmin.hello.micronaut.command.EmployeeSaveCommand;
import ro.hume.cosmin.hello.micronaut.dao.FilterAndSortArguments;
import ro.hume.cosmin.hello.micronaut.domain.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {

    Employee create(EmployeeSaveCommand cmd);

    Optional<Employee> get(Long id);

    List<Employee> list(FilterAndSortArguments args);

    void delete(Long id);
}
