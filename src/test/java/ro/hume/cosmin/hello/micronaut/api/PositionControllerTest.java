package ro.hume.cosmin.hello.micronaut.api;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.Test;
import ro.hume.cosmin.hello.micronaut.command.PositionSaveCommand;
import ro.hume.cosmin.hello.micronaut.domain.Position;

import javax.inject.Inject;

import static io.micronaut.http.HttpRequest.PUT;
import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
class PositionControllerTest {

    @Inject
    EmbeddedServer server;

    @Inject
    @Client("/position")
    HttpClient client;

    @Test
    void testCreatePositionResponse() {
        var response = client.toBlocking()
                .exchange(
                        PUT("/", new PositionSaveCommand("dev")),
                        Position.class
                );
        assertEquals(HttpStatus.CREATED.getCode(), response.code());
    }

    @Test
    void testGetPositionResponse() {
        var blockingClient = client.toBlocking();
        var request = HttpRequest.GET("/0");

        HttpClientResponseException thrown = assertThrows(
                HttpClientResponseException.class,
                () -> blockingClient.exchange(request)
        );
        assertNotNull(thrown.getResponse());
        assertEquals(HttpStatus.NOT_FOUND, thrown.getStatus());
    }
}
