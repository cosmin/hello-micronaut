package ro.hume.cosmin.hello.micronaut.dao;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EmployeeQueryBuilderTest {

    private final QueryBuilder queryBuilder = new EmployeeQueryBuilder();

    @Test
    void testFindAllQueryNoArgs() {
        FilterAndSortArguments args = new FilterAndSortArguments();
        String query = queryBuilder.findAllQuery(args);
        assertEquals("SELECT e FROM Employee AS e", query);
    }

    @Test
    void testFindAllQueryFilterOnly() {
        FilterAndSortArguments args = new FilterAndSortArguments();
        args.setPosition(1L);
        String query = queryBuilder.findAllQuery(args);
        assertEquals("SELECT e FROM Employee AS e WHERE e.position = 1", query);
    }

    @Test
    void testFindAllQuerySortOnly() {
        FilterAndSortArguments args = new FilterAndSortArguments();
        args.setSort("id");
        String query = queryBuilder.findAllQuery(args);
        assertEquals("SELECT e FROM Employee AS e ORDER BY e.id", query);
    }

    @Test
    void testFindAllQuerySortWithOrder() {
        FilterAndSortArguments args = new FilterAndSortArguments();
        args.setSort("id");
        args.setOrder("desc");
        String query = queryBuilder.findAllQuery(args);
        assertEquals("SELECT e FROM Employee AS e ORDER BY e.id DESC", query);
    }

    @Test
    void testFindAllQueryFilterAndSort() {
        FilterAndSortArguments args = new FilterAndSortArguments();
        args.setPosition(1L);
        args.setSort("id");
        args.setOrder("desc");
        String query = queryBuilder.findAllQuery(args);
        assertEquals("SELECT e FROM Employee AS e WHERE e.position = 1 ORDER BY e.id DESC", query);
    }
}
