package ro.hume.cosmin.hello.micronaut.api;

import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import ro.hume.cosmin.hello.micronaut.command.EmployeeSaveCommand;
import ro.hume.cosmin.hello.micronaut.command.PositionSaveCommand;
import ro.hume.cosmin.hello.micronaut.domain.Employee;
import ro.hume.cosmin.hello.micronaut.domain.Position;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import static io.micronaut.http.HttpRequest.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@MicronautTest
class EmployeeControllerTest {

    @Inject
    EmbeddedServer server;

    @Inject
    @Client("/")
    HttpClient client;

    private final List<Long> positionIds = new ArrayList<>();
    private final Deque<Long> employeeIds = new ArrayDeque<>();

    @AfterEach
    void cleanUp() {
        while (!employeeIds.isEmpty()) {
            Long employeeId = employeeIds.removeFirst();
            var request = DELETE("/employee/" + employeeId);
            var response = client.toBlocking().exchange(request);
            assertEquals(HttpStatus.NO_CONTENT, response.getStatus());
        }
        for (Long positionId : positionIds) {
            var request = DELETE("/position/" + positionId);
            var response = client.toBlocking().exchange(request);
            assertEquals(HttpStatus.NO_CONTENT, response.getStatus());
        }
    }

    @Test
    void testCreateManagerEmployeeResponse() {
        var managerId = createPosition("manager");
        var manager = EmployeeSaveCommand.builder()
                .position(managerId)
                .name("Robinson")
                .startDate(LocalDate.now())
                .endDate(LocalDate.now())
                .build();

        var response = client.toBlocking()
                .exchange(
                        PUT("/employee", manager),
                        Employee.class
                );
        assertEquals(HttpStatus.CREATED.getCode(), response.code());
        employeeIds.push(response.getBody().orElseThrow().getId());
    }

    @Test
    void testCreateWorkerEmployeeResponse() {
        var managerPositionId = createPosition("manager");
        var managerId = createEmployee("Robinson", managerPositionId);
        var workerPositionId = createPosition("worker");
        var worker = EmployeeSaveCommand.builder()
                .name("Friday")
                .position(workerPositionId)
                .superior(managerId)
                .startDate(LocalDate.now())
                .endDate(LocalDate.now())
                .build();

        var response = client.toBlocking()
                .exchange(
                        PUT("/employee", worker),
                        Employee.class
                );
        assertEquals(HttpStatus.CREATED.getCode(), response.code());
        employeeIds.push(response.getBody().orElseThrow().getId());
    }

    private long createPosition(String name) {
        var response = client.toBlocking()
                .exchange(
                        PUT("/position", new PositionSaveCommand(name)),
                        Position.class
                );
        var id = response.getBody().orElseThrow().getId();
        positionIds.add(id);
        return id;
    }

    private long createEmployee(String name, long positionId) {
        var employee = EmployeeSaveCommand.builder()
                .name(name)
                .position(positionId)
                .startDate(LocalDate.now())
                .endDate(LocalDate.now())
                .build();
        var response = client.toBlocking()
                .exchange(
                        PUT("/employee", employee),
                        Employee.class
                );
        var id = response.getBody().orElseThrow().getId();
        employeeIds.push(id);
        return id;
    }

    @Test
    void testGetEmployeeResponse() {
        var managerPositionId = createPosition("manager");
        var managerId = createEmployee("Robinson", managerPositionId);

        HttpResponse<Employee> response = client.toBlocking()
                .exchange(GET("/employee/" + managerId), Employee.class);
        Employee employee = response.getBody().orElseThrow();

        assertEquals("Robinson", employee.getName());
    }

    @Test
    void testListEmployeesResponse() {
        var managerPositionId = createPosition("manager");
        createEmployee("Robinson", managerPositionId);
        var workerPositionId = createPosition("worker");
        createEmployee("Friday", workerPositionId);

        List<Employee> employees = client.toBlocking()
                .retrieve(
                        GET("/employee?sort=name&order=desc"),
                        Argument.of(List.class, Employee.class)
                );

        assertEquals(2, employees.size());
        assertEquals("Robinson", employees.get(0).getName());
    }

    @Test
    void testListAndFilterEmployeesResponse() {
        var managerPositionId = createPosition("manager");
        createEmployee("Robinson", managerPositionId);
        var workerPositionId = createPosition("worker");
        createEmployee("Friday", workerPositionId);

        List<Employee> employees = client.toBlocking()
                .retrieve(
                        GET("/employee?position=" + managerPositionId + "&sort=name&order=desc"),
                        Argument.of(List.class, Employee.class)
                );

        assertEquals(1, employees.size());
        assertEquals("Robinson", employees.get(0).getName());
    }
}
