package ro.hume.cosmin.hello.micronaut.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import ro.hume.cosmin.hello.micronaut.command.EmployeeSaveCommand;
import ro.hume.cosmin.hello.micronaut.dao.EmployeeRepository;
import ro.hume.cosmin.hello.micronaut.domain.Employee;
import ro.hume.cosmin.hello.micronaut.domain.Position;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class DefaultEmployeeServiceTest {

    @InjectMocks
    private DefaultEmployeeService employeeService;

    @Mock
    private EmployeeRepository employeeRepository;

    @Mock
    private PositionService positionService;

    @Spy
    private ModelMapper modelMapper = new ModelMapper();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testMappingOnCreate() {
        var workerPosition = Position.builder().id(1L).name("worker").build();
        var managerPosition = Position.builder().id(2L).name("manager").build();
        var manager = Employee.builder().id(1L).name("Robinson").position(managerPosition).build();
        var start = LocalDate.of(2021, 4, 10);
        var end = LocalDate.of(2021, 4, 11);
        var cmd = EmployeeSaveCommand.builder()
                .name("Friday")
                .position(workerPosition.getId())
                .superior(manager.getId())
                .startDate(start)
                .endDate(end)
                .build();
        when(positionService.get(workerPosition.getId())).thenReturn(Optional.of(workerPosition));
        when(employeeRepository.findById(manager.getId())).thenReturn(Optional.of(manager));
        when(employeeRepository.save(any())).then(returnsFirstArg());

        var employee = employeeService.create(cmd);

        assertEquals(
                Employee.builder()
                        .name("Friday")
                        .position(workerPosition)
                        .superior(manager)
                        .startDate(start)
                        .endDate(end)
                        .build(),
                employee
        );
    }
}
